package src;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JTextField;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PersonPanel extends JPanel implements PropertyChangeListener{
	protected Person model = null;
	// create components
	private		JLabel nameLabel = new JLabel("navn: ");
	protected 	JTextField NamePropertyComponent = new JTextField(20);
	private		JLabel emailLabel = new JLabel("email: ");
	private		JTextField EmailPropertyComponent = new JTextField(20);
	private		JLabel birthdayLabel = new JLabel("Fødselsdag: ");
	private		JTextField DateOfBirthPropertyComponent = new JTextField(20);
	private		JLabel genderLabel = new JLabel("Køn: ");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private		JComboBox GenderPropertyComponent = new JComboBox(Gender.values());
	private		JLabel heightLabel = new JLabel("Højde: ");
	static final int HEIGHT_MIN = 50;
	static final int HEIGHT_MAX = 250;
	static final int HEIGHT_INIT = 189;
	private		JSlider HeightPropertyComponent = new JSlider(JSlider.HORIZONTAL,
			HEIGHT_MIN, HEIGHT_MAX, HEIGHT_INIT);
	protected 	SpringLayout layout = new SpringLayout();
	
	private ArrayList<JComponent> compArray = new ArrayList<JComponent>();
	
	
	//Constructor:
	public PersonPanel(){	
		this.model = new Person();
		model.addPropertyChangeListener(this);
		//J-slider stuff:
		getHeightPropertyComponent().setMajorTickSpacing(50);
		getHeightPropertyComponent().setMinorTickSpacing(10);
		getHeightPropertyComponent().setPaintTicks(true);
		getHeightPropertyComponent().setPaintLabels(true);
		getHeightPropertyComponent().setSize(new Dimension(300, 5));
		
		//add changePropListeners
		getNamePropertryComponent().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setName(evt.getActionCommand());
				System.out.println(model.getName());
			}
		});
		
		getEmailPropertyComponent().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setEmail(evt.getActionCommand());
				System.out.println(model.getEmail());
			}
		});
		getDateOfBirthPropertyComponent().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setDateOfBirth(evt.getActionCommand());
				System.out.println(model.getDateOfBirth());
			}
		});
		getGenderPropertyComponent().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Gender newGender = (Gender) getGenderPropertyComponent().getSelectedItem();
				model.setGender(newGender);
				System.out.println("new gender: " + model.getGender());
			}
		});
		getHeightPropertyComponent().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				model.setHeight(getHeightPropertyComponent().getValue());
				System.out.println(model.getHeight()); 
			}
		});
		
		//add comps to array
		getCompArray().add(nameLabel);
		getCompArray().add(getNamePropertryComponent());
		getCompArray().add(emailLabel);
		getCompArray().add(getEmailPropertyComponent());
		getCompArray().add(birthdayLabel);
		getCompArray().add(getDateOfBirthPropertyComponent());
		getCompArray().add(genderLabel);
		getCompArray().add(getGenderPropertyComponent());
		getCompArray().add(heightLabel);
		getCompArray().add(getHeightPropertyComponent());
		
		//set layout
		this.setLayout(layout);
        
		
        for(int i = 0; i < getCompArray().size(); i++){
        	// add comps to this panel
        	this.add(getCompArray().get(i));
        	
        	//add layout constraints
        	if( i == 0){
        		layout.putConstraint(SpringLayout.WEST, getCompArray().get(i),
            			20,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,getCompArray().get(i),
        				50,
                        SpringLayout.NORTH, this);
        	}
        	else if( i % 2 == 0){
        		layout.putConstraint(SpringLayout.WEST, getCompArray().get(i),
            			20,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,getCompArray().get(i),
        				(50 * i + 50),
                        SpringLayout.NORTH, this);
        	}
        	else{
        		int x = i - 1;
        		layout.putConstraint(SpringLayout.WEST, getCompArray().get(i),
            			120,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,getCompArray().get(i),
        				(50 * x + 50),
                        SpringLayout.NORTH, this);
        	}
        	
        	
        	
        }
	}
	
	public void setModel(Person p){
		this.model = p;
		this.getNamePropertryComponent().setText(this.model.getName());
		this.getEmailPropertyComponent().setText(this.model.getEmail());
		this.getDateOfBirthPropertyComponent().setText(this.model.getDateOfBirth());
		this.getGenderPropertyComponent().setSelectedItem(model.getGender());
		this.getHeightPropertyComponent().setValue(this.model.getHeight());
	}
	
	public Person getModel(){
		return this.model;
	}

	public JTextField getNamePropertryComponent() {
		return NamePropertryComponent;
	}

	public void setNamePropertryComponent(JTextField namePropertryComponent) {
		NamePropertryComponent = namePropertryComponent;
	}

	public JTextField getEmailPropertyComponent() {
		return EmailPropertyComponent;
	}

	public void setEmailPropertyComponent(JTextField emailPropertyComponent) {
		EmailPropertyComponent = emailPropertyComponent;
	}

	public JTextField getDateOfBirthPropertyComponent() {
		return DateOfBirthPropertyComponent;
	}

	public void setDateOfBirthPropertyComponent(
			JTextField dateOfBirthPropertyComponent) {
		DateOfBirthPropertyComponent = dateOfBirthPropertyComponent;
	}

	public JSlider getHeightPropertyComponent() {
		return HeightPropertyComponent;
	}

	public void setHeightPropertyComponent(JSlider heightPropertyComponent) {
		HeightPropertyComponent = heightPropertyComponent;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getGenderPropertyComponent() {
		return GenderPropertyComponent;
	}

	@SuppressWarnings("rawtypes")
	public void setGenderPropertyComponent(JComboBox genderPropertyComponent) {
		GenderPropertyComponent = genderPropertyComponent;
	}
	
	public ArrayList<JComponent> getCompArray() {
		return compArray;
	}

	public void setCompArray(ArrayList<JComponent> compArray) {
		this.compArray = compArray;
	}

	public static Person thea = new Person("thea", "070685", Gender.female, "thea@mail.com", 162);


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() == Person.NAME_PROPERTY) {
        	NamePropertyComponent.setText(model.getName());
        }
        else if (evt.getPropertyName() == Person.DATE_PROPERTY) {
            DateOfBirthPropertyComponent.setText(model.getDateOfBirth());
        }
//      else if (evt.getPropertyName() == Person.GENDER_PROPERTY) {
//          GenderPropertyComponent.setSelectedItem(model.getGender());
//      }
        else if (evt.getPropertyName() == Person.EMAIL_PROPERTY) {
            EmailPropertyComponent.setText(model.getEmail());
        }
//      else if (evt.getPropertyName() == Integer.toString(Person.HEIGHT_PROPERTY)) {
//          HeightPropertyComponent.setValue(model.getHeight());
//      } 
		
	}
	
}
