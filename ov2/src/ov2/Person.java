package ov2;

import java.beans.PropertyChangeSupport;

public class Person {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private String name;
	private String dateOfBirth;
	private Gender gender;
	private String email;
	private Integer height;
	
	public Person(String name, String dateOfBirth, Gender gender, String email, int height) {
		super();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
		this.email = email;
		this.height = height;
	}
	
	public Person(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldValue = this.name;
        this.name = name;
        this.pcs.firePropertyChange("name", oldValue, name);
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String newValue) {
		String oldValue = this.dateOfBirth;
        this.dateOfBirth = newValue;
        this.pcs.firePropertyChange("dateOfBirth", oldValue, newValue);
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender newValue) {
		Gender oldValue = this.gender;
        this.gender = newValue;
        this.pcs.firePropertyChange("gender", oldValue, newValue);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String newValue) {
		String oldValue = this.email;
        this.email = newValue;
        this.pcs.firePropertyChange("email", oldValue, newValue);
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int newValue) {
		int oldValue = this.height;
        this.height = newValue;
        this.pcs.firePropertyChange("height", oldValue, newValue);
	}
	
	public Person getPerson(){
		return this;
	}
	
}
