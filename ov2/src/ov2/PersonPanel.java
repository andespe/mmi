package ov2;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class PersonPanel extends JPanel{
	private Person model = null;
	// create components
	private	JLabel nameLabel = new JLabel("navn: ");
	private	JTextField NamePropertryComponent = new JTextField(20);
	private	JLabel emailLabel = new JLabel("email: ");
	private	JTextField EmailPropertyComponent = new JTextField(20);
	private	JLabel birthdayLabel = new JLabel("Fødselsdag: ");
	private	JTextField DateOfBirthPropertyComponent = new JTextField(20);
	private	JLabel genderLabel = new JLabel("Køn: ");
	private	JComboBox GenderPropertyComponent = new JComboBox(Gender.values());
	private	JLabel heightLabel = new JLabel("Højde: ");
	static final int HEIGHT_MIN = 50;
	static final int HEIGHT_MAX = 250;
	static final int HEIGHT_INIT = 189;
	private	JSlider HeightPropertyComponent = new JSlider(JSlider.HORIZONTAL,
			HEIGHT_MIN, HEIGHT_MAX, HEIGHT_INIT);
	
	private ArrayList<JComponent> compArray = new ArrayList<JComponent>();
	
	//Constructor:
	public PersonPanel(){	
		//J-slider stuff:
		HeightPropertyComponent.setMajorTickSpacing(50);
		HeightPropertyComponent.setMinorTickSpacing(10);
		HeightPropertyComponent.setPaintTicks(true);
		HeightPropertyComponent.setPaintLabels(true);
		HeightPropertyComponent.setSize(new Dimension(300, 5));
		
		//add changePropListeners
		NamePropertryComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setName(evt.getActionCommand());
				System.out.println(model.getName());
			}
		});
		EmailPropertyComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setEmail(evt.getActionCommand());
				System.out.println(model.getEmail());
			}
		});
		DateOfBirthPropertyComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				model.setDateOfBirth(evt.getActionCommand());
				System.out.println(model.getDateOfBirth());
			}
		});
		GenderPropertyComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Gender newGender = (Gender) GenderPropertyComponent.getSelectedItem();
				model.setGender(newGender);
				System.out.println("new gender: " + model.getGender());
			}
		});
		HeightPropertyComponent.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				model.setHeight(HeightPropertyComponent.getValue());
				System.out.println(model.getHeight()); 
			}
		});
		
		//add comps to array
		compArray.add(nameLabel);
		compArray.add(NamePropertryComponent);
		compArray.add(emailLabel);
		compArray.add(EmailPropertyComponent);
		compArray.add(birthdayLabel);
		compArray.add(DateOfBirthPropertyComponent);
		compArray.add(genderLabel);
		compArray.add(GenderPropertyComponent);
		compArray.add(heightLabel);
		compArray.add(HeightPropertyComponent);
		//set layout
		SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        
		
        for(int i = 0; i < compArray.size(); i++){
        	// add comps to this panel
        	this.add(compArray.get(i));
        	
        
        	
        	//add layout constraints
        	if( i == 0){
        		layout.putConstraint(SpringLayout.WEST, compArray.get(i),
            			20,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,compArray.get(i),
        				50,
                        SpringLayout.NORTH, this);
        	}
        	else if( i % 2 == 0){
        		layout.putConstraint(SpringLayout.WEST, compArray.get(i),
            			20,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,compArray.get(i),
        				(50 * i + 50),
                        SpringLayout.NORTH, this);
        	}
        	else{
        		int x = i - 1;
        		layout.putConstraint(SpringLayout.WEST, compArray.get(i),
            			120,
                        SpringLayout.WEST, this);
        		layout.putConstraint(SpringLayout.NORTH,compArray.get(i),
        				(50 * x + 50),
                        SpringLayout.NORTH, this);
        	}
        	
        	
        	
        }
	}
	
	public void setModel(Person p){
		this.model = p;
		this.NamePropertryComponent.setText(this.model.getName());
		this.EmailPropertyComponent.setText(this.model.getEmail());
		this.DateOfBirthPropertyComponent.setText(this.model.getDateOfBirth());
		this.GenderPropertyComponent.setSelectedItem(model.getGender());
		this.HeightPropertyComponent.setValue(this.model.getHeight());
	}
	
	
	
	public static void main(String[] args) {
		//Create and set up the window.
		final JFrame frame = new JFrame("MMI Oving 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(400,650));
		
		//test-person
//		Person anders = new Person("anders", "161184", Gender.male, "anders@mail.com", 189);
		Person thea = new Person("thea", "070685", Gender.female, "thea@mail.com", 162);
	
		//Add Panel to the frame and display the window.
		PersonPanel panel = new PersonPanel();
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		panel.setModel(thea);

	}

}
