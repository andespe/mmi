package src;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {
	
	public static void main(String[] args) {
		//Create and set up the window.
		final JFrame frame = new JFrame("MMI Oving 3");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(400,1000));
		
		//Add Panel to the frame and display the window.
		PersonPanel panel = new PersonPanel();
		PassivePersonPanel pasPanel = new PassivePersonPanel(panel.getModel());
		frame.add(panel);
		frame.add(pasPanel);
		frame.pack();
		frame.setVisible(true);
		
	}
}
