package src;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.*;

@SuppressWarnings("serial")
public class PassivePersonPanel extends PersonPanel implements PropertyChangeListener{
    private JTextField NamePropertyComponent = new JTextField(20);
    private JTextField EmailPropertyComponent = new JTextField(20);
    private JTextField DateOfBirthPropertyComponent = new JTextField(20);
    private JTextField GenderPropertyComponent = new JTextField(20);
    private JTextField HeightPropertyComponent = new JTextField(20);
    private Person model; 
	private ArrayList<JComponent> passiveCompArray = new ArrayList<JComponent>();
	
	//Constructor:
	public PassivePersonPanel(Person p){	
		this.model = p;
		model.addPropertyChangeListener(this);
		//add comps to array
		passiveCompArray.add(NamePropertyComponent);
		passiveCompArray.add(EmailPropertyComponent);
		passiveCompArray.add(DateOfBirthPropertyComponent);
		passiveCompArray.add(GenderPropertyComponent);
		passiveCompArray.add(HeightPropertyComponent);
		// add text to all fields
		
		for(int i = 0; i < passiveCompArray.size(); i++){
			//set all components to non-editable (disable)
			passiveCompArray.get(i).setEnabled(false);
			// set layout constraints
			layout.putConstraint(SpringLayout.WEST, passiveCompArray.get(i),
        			20,
                    SpringLayout.WEST, this);
			layout.putConstraint(SpringLayout.NORTH,passiveCompArray.get(i),
    				((500 + ((i + 1) * 50)) ),
                    SpringLayout.NORTH, this);
			//add comps to panel
			this.add(passiveCompArray.get(i));
		}
		

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("passive : " + evt);
		if (evt.getPropertyName() == Person.NAME_PROPERTY) {
            this.NamePropertyComponent.setText((String) evt.getNewValue());
        }
        else if (evt.getPropertyName() == Person.DATE_PROPERTY) {
        	this.DateOfBirthPropertyComponent.setText((String) evt.getNewValue());
        }
        else if (evt.getPropertyName() == Person.GENDER_PROPERTY) {
        	String gender = evt.getNewValue().toString();
            this.GenderPropertyComponent.setText(gender);
        }
        else if (evt.getPropertyName() == Person.EMAIL_PROPERTY) {
            this.EmailPropertyComponent.setText((String) evt.getNewValue());
        }
        else if (evt.getPropertyName() == Person.HEIGHT_PROPERTY) {
        	String height = evt.getNewValue().toString();
            HeightPropertyComponent.setText(height);
        } 
	}

}