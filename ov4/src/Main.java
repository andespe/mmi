package src;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Main {
	
	public static void main(String[] args) {
		//Create and set up the window.
		final JFrame frame = new JFrame("MMI Oving 4");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(400,700));
		
		PersonListPanel pPanel = new PersonListPanel();
		
		//Add Panel to the frame and display the window.
		frame.add(pPanel);
		frame.pack();
		frame.setVisible(true);
		
	}
}
