package src;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Person {
    public static String NAME_PROPERTY = "NAME_PROPERTY";
    public static String DATE_PROPERTY = "DATE_PROPERTY";
    public static String GENDER_PROPERTY = "GENDER_PROPERTY";
    public static String EMAIL_PROPERTY = "EMAIL_PROPERTY";
    public static String HEIGHT_PROPERTY = "HEIGHT_PROPERTY"; 
	private String name;
	private String dateOfBirth;
	private Gender gender;
	private String email;
	private int height;
	private PropertyChangeSupport pcs;
	
	//constructors
    public Person() {
        pcs = new PropertyChangeSupport(this);
    } 
    
    public Person(String name, String date, Gender gender, String email, Integer height){
    	pcs = new PropertyChangeSupport(this);
    	this.name = name;
    	this.dateOfBirth = date;
    	this.gender = gender;
    	this.email = email;
    	this.height = height;
    }
	
	//addPropertyChangeListeners functions
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

    
    //getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.name;
        this.name = name;
		this.pcs.firePropertyChange(NAME_PROPERTY, oldName, name);
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
        String oldValue = this.dateOfBirth;
        this.dateOfBirth = dateOfBirth;
        this.pcs.firePropertyChange(DATE_PROPERTY, oldValue, dateOfBirth);
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		Gender oldValue = this.gender;
		this.gender = gender;
        this.pcs.firePropertyChange(GENDER_PROPERTY, oldValue, gender);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
        String oldValue = this.email;
        this.email = email;
        this.pcs.firePropertyChange(EMAIL_PROPERTY, oldValue, email);
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
        String oldValue = new Integer(height).toString();
        this.height = height;
        this.pcs.firePropertyChange(HEIGHT_PROPERTY, oldValue, height);
	}
	
	public Person getPerson(){
		return this;
	}
	
}
