package src;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

@SuppressWarnings("serial")
public class PersonListPanel extends JPanel{
	PersonPanel PersonPanel;
	@SuppressWarnings("rawtypes")
	DefaultListModel listModel;
	@SuppressWarnings({ "rawtypes" })
	JList PersonList;
	JScrollPane pane;
	JButton addButton;
	JButton removeButton;
	
	//constructor
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PersonListPanel(){
		//set layout
		this.setLayout(new GridLayout(1,4));
		// define fields
		PersonPanel = new PersonPanel();
		PersonPanel.setName("PersonPanel");
		listModel = new DefaultListModel();
		PersonList = new JList(listModel);
		PersonList.setCellRenderer(new PersonRenderer());
//		PersonList.setSelectionModel();
		PersonList.setName("PersonList");
		pane = new JScrollPane(PersonList);
	    addButton = new JButton("Add Element");
	    removeButton = new JButton("Remove Element");
	    // add elements to the panel
	    this.add(PersonPanel);
		this.add(pane);
		this.add(addButton);
		this.add(removeButton);
		
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// make new person instance and add it to the list
				Person p = new Person();
				p.setName(PersonPanel.NamePropertyComponent.getText());
				p.setDateOfBirth(PersonPanel.DateOfBirthPropertyComponent.getText());
				p.setEmail(PersonPanel.EmailPropertyComponent.getText());
				p.setGender((Gender) PersonPanel.GenderPropertyComponent.getSelectedItem());
				p.setHeight(PersonPanel.HeightPropertyComponent.getValue());
				listModel.addElement(p);
				PersonPanel.NamePropertyComponent.setText("");
				PersonPanel.DateOfBirthPropertyComponent.setText("");
				PersonPanel.EmailPropertyComponent.setText("");
			}
		});
		
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Person p = (Person) PersonList.getSelectedValue();
				listModel.removeElement(p);
			}
		});
		
		PersonList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if((Person) PersonList.getSelectedValue() != null){
					PersonPanel.setModel((Person) PersonList.getSelectedValue());
				}else{
					PersonPanel.setModel(new Person());
				}
			}
		});
		
	}
	
	@SuppressWarnings("unchecked")
	public void setModel(DefaultListModel<?> dlm){
		this.PersonList.setModel(dlm);
	}
	
	public DefaultListModel<?> getModel() {
		return (DefaultListModel<?>) this.PersonList.getModel();
	}

}
