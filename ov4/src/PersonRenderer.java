package src;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

@SuppressWarnings("serial")
public class PersonRenderer extends DefaultListCellRenderer{
	
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		ImageIcon icon = null;
		JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof Person) {
        	if(((Person)value).getGender() == Gender.male){
        		icon = new ImageIcon(getClass().getResource("/male.png"));
        	}else{
        		icon = new ImageIcon(getClass().getResource("/female.png"));
        	}
        	String s = ((Person)value).getName() + ", " + ((Person)value).getEmail();
        	label.setIcon(icon);
            label.setText(s);
        }
        return this;
    }

}
